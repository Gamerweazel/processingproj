ArrayList < Slice > slices;
Slice slice;
float start = 0, fin = 1.0472, amt = 0.1;
boolean spin = false;
int direction = 1;

void setup() {
		size(600, 600);
		strokeWeight(3);
		frameRate(60);
		background(60);
		slices = new ArrayList < Slice >();
		for (int i = 0; i < 6; i++) {
				float r = random(255), g = random(255), b = random(255);
				slice = new Slice( start, fin, r, g, b );
				slices.add(slice);
				start += 1.0472;
				fin += 1.0472;
		}
}

void draw() {
		if (spin == false) {
				background( get(width / 2, height / 2 + 1) );
		}
		if (spin == true) {
				if (amt <= 0.01) {
						amt = 0;
						spin = false;
				}
				if (frameCount % 6 == 0) {
						amt *= 0.9;
				}
		}
		for (Slice s : slices) {
				s.show();
				if (spin == true) {
						s.spin(direction, amt);
				}
		}
}

void mousePressed() {
		spin = false;
}

void mouseDragged() {
		if (pmouseY < mouseY) {
				direction = 1;
		} else {
				direction = -1;
		}
		for (Slice s : slices) {
				s.drag(direction);
		}
}

void mouseWheel(MouseEvent event) {
		int d = event.getCount();
		for (Slice s : slices) {
				s.drag(d);
		}
}

void mouseReleased() {
		spin = true;
		amt = Math.abs(pmouseY - mouseY) / 6;
}
