class Slice {
int x, y;
float start, fin, r, g, b;
public Slice(float start, float fin, float r, float g, float b) {
		this.start = start;
		this.fin = fin;
		this.r = r;
		this.g = g;
		this.b = b;
		this.x = width / 2;
		this.y = height / 2;
}

public void show() {
		fill(this.r, this.g, this.b);
		arc( this.x, this.y, 500, 500, this.start, this.fin);
}

public void spin(int d, float a) {
		if (d == 1) {
				this.start += a;
				this.fin += a;
		} else {
				this.start -= a;
				this.fin -= a;
		}

}

public void drag(int dir) {
		if (dir == 1) {
				this.start += 0.1;
				this.fin += 0.1;
		} else {
				this.start -= 0.1;
				this.fin -= 0.1;
		}
}
}
