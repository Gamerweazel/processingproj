import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class carouselJava extends PApplet {

ArrayList < Slice > slices;
Slice slice;
float start = 0, fin = 1.0472f, amt = 0.1f;
boolean spin = false;
int direction = 1;

public void setup() {
		
		strokeWeight(3);
		frameRate(60);
		background(60);
		slices = new ArrayList < Slice >();
		for (int i = 0; i < 6; i++) {
				float r = random(255), g = random(255), b = random(255);
				slice = new Slice( start, fin, r, g, b );
				slices.add(slice);
				start += 1.0472f;
				fin += 1.0472f;
		}
}

public void draw() {
		if (spin == false) {
				background( get(width / 2, height / 2 + 1) );
		}
		if (spin == true) {
				if (amt <= 0.01f) {
						amt = 0;
						spin = false;
				}
				if (frameCount % 6 == 0) {
						amt *= 0.9f;
				}
		}
		for (Slice s : slices) {
				s.show();
				if (spin == true) {
						s.spin(direction, amt);
				}
		}
}

public void mousePressed() {
		spin = false;
}

public void mouseDragged() {
		if (pmouseY < mouseY) {
				direction = 1;
		} else {
				direction = -1;
		}
		for (Slice s : slices) {
				s.drag(direction);
		}
}

public void mouseWheel(MouseEvent event) {
		int d = event.getCount();
		for (Slice s : slices) {
				s.drag(d);
		}
}

public void mouseReleased() {
		spin = true;
		amt = Math.abs(pmouseY - mouseY) / 6;
}
class Slice {
int x, y;
float start, fin, r, g, b;
public Slice(float start, float fin, float r, float g, float b) {
		this.start = start;
		this.fin = fin;
		this.r = r;
		this.g = g;
		this.b = b;
		this.x = width / 2;
		this.y = height / 2;
}

public void show() {
		fill(this.r, this.g, this.b);
		arc( this.x, this.y, 500, 500, this.start, this.fin);
}

public void spin(int d, float a) {
		if (d == 1) {
				this.start += a;
				this.fin += a;
		} else {
				this.start -= a;
				this.fin -= a;
		}

}

public void drag(int dir) {
		if (dir == 1) {
				this.start += 0.1f;
				this.fin += 0.1f;
		} else {
				this.start -= 0.1f;
				this.fin -= 0.1f;
		}
}
}
  public void settings() { 	size(600, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "carouselJava" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
