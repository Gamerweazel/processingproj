const path = require('path');

module.exports = {
    entry: [
        './src/sketch',
        './src/slice'
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    debug: true,
    devtool: 'source-map',
    module: {
        loaders: [{
            loader: 'babel-loader',
            include: [
                path.resolve(__dirname, 'src')
            ],
            test: /\.js?$/,
            query: {
                presets: ['es2015']
            }
        }]
    }
};
