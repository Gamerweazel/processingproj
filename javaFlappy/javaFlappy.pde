Bird bird;

public void setup() {
	size(600, 600);
	bird = new Bird();
}

public void draw() {
	background(50);
	bird.show();
}
