import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class javaFlappy extends PApplet {

Bird bird;

public void setup() {
	
	bird = new Bird();
}

public void draw() {
	background(50);
	bird.show();
}
class Bird {
float gravity = 0.6f, velocity = 0.0f;
int x = 64, y = height/2;

public void Bird() {
	this.x = x;
	this.y = y;
	this.gravity = gravity;
	this.velocity = velocity;
}

public void show() {
	fill(0, 255, 0);
	ellipse(this.x, this.y, 20, 20);
}
}
  public void settings() { 	size(600, 600); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "javaFlappy" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
