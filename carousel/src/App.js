import React, { Component } from 'react'
import Menu from './components/common/Menu'
import P5Component from './components/P5Component'
import { coloursel } from './sketches/coloursel'
import { grad } from './sketches/grad'
import { colourPop } from './sketches/colourPop'

export default class App extends Component {
    constructor() {
        super()
        this.state = {
            selectedSketch: coloursel,
            menuOpen: false
        }
    }

    showMenu() {
        this.refs.menu.show();
        let menuState = this.state.menuOpen ? false : true;
        this.setState({menuOpen: menuState})
    }

    changeSketch(item) {
        this.setState({selectedSketch: item});
    }

    changeHead() {
        if (this.state.selectedSketch == coloursel) return 'Coloursel'
        else if (this.state.selectedSketch == grad) return 'Gradients'
        else if (this.state.selectedSketch == colourPop) return 'ColourPop'
    }

    makeColourPopAvailable() {
        if (!(/Android|webOS|iPhone|iPad|Mobile|iPod|BlackBerry/i.test(navigator.userAgent))) {
            return (<div className='listItems'
                onClick={() => this.changeSketch(colourPop)}>ColourPop</div>)
        }
        else return (<p>ColourPop is not currently available on mobile devices</p>)
    }

    render() {
        return(
            <div>
                <div className='head'>{this.changeHead()}</div>
                <div className='burgerWrapper' onClick={() => this.showMenu()}>
                    <div className='burger'></div>
                    <div className='burger'></div>
                    <div className='burger'></div>
                </div>
                <P5Component sketch={this.state.selectedSketch}
                    menuOpen={this.state.menuOpen} />
                <Menu ref='menu'>
                    <div className='listItems' onClick={() => this.changeSketch(coloursel)}>Coloursel</div>
                    <div className='listItems' onClick={() => this.changeSketch(grad)}>Gradients</div>
                    {this.makeColourPopAvailable()}
                    <p>Made with love using p5JS and React</p>
                </Menu>
            </div>
        )
    }
}
