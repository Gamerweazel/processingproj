export default class Dot {
    constructor(x, y, w, r, g, b, a, tarR, tarG, tarB, dx, dy, id, p) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.radius = this.w/2;
        this.history = [];
        this.id = id;
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
        this.textAlpha = -100;
        this.winner = false;
        this.diff = 255;
        this.dX = dx;
        this.dY = dy;
        this.drag = 0.1;

        this.show = () => {
            p.fill(this.r, this.g, this.b, this.a);
            p.ellipse(this.x, this.y, this.w);
            // if (this.id) {
            //     p.fill(0,0,0, 255);
            //     p.text(`${this.id}`, this.x, this.y)
            // }
            return
        }

        this.move = () => {
            this.history.push([this.x, this.y]);
            this.x += this.dX;
            this.y += this.dY;
            if (this.x < this.radius) {
                this.x = this.radius;
                this.dX = -this.dX * this.drag;
            } else if (this.x > p.width - this.radius) {
                this.x = p.width - this.radius;
                this.dX = -this.dX * this.drag;
            } else if (this.y < this.radius) {
                this.y = this.radius
                this.dY = -this.dY * this.drag;
            } else if (this.y > p.height - this.radius) {
                this.y = p.height - this.radius
                this.dY = -this.dY * this.drag
            }
            if (this.history.length > 20) {
                this.history.splice(0, 1);
            }
            return
        }

        this.drawRing = () => {
            p.ellipse(this.x, this.y, this.w * 1.5);
            return
        }

        this.drawTail = (size = 2) => {
            for (let i = 0; i < this.history.length; i+=3) {
                p.fill(this.r, this.g, this.b, this.a*.3);
                p.ellipse(this.history[i][0], this.history[i][1], i * size);
            }
            return
        }

        this.setDiff = () => {
            this.diff = p.abs(p.abs(tarR - this.r) +
                p.abs(tarG - this.g) +
                p.abs(tarB - this.b))
        }

        this.drawStats = () => {
            p.fill(50,50,50, this.textAlpha);
            p.text(`The target value was R:${tarR}, G:${tarG}, B:${tarB}`, this.x, p.height/7);
            p.fill(50,50,50, this.textAlpha - 50);
            p.text(`This dot's value is R:${this.r}, G:${this.g}, B:${this.b}`, this.x, p.height/4);
            p.fill(50,50,50, this.textAlpha - 100);
            p.text(`This dot was off by ${this.diff} values`, this.x, p.height/1.2);
            return
        }

        this.drawColor = (tarR, tarG, tarB) => {
            p.text(`${this.diff}`, this.x, this.y - 50);
            return
        }

        this.moveTo = (targetX, targetY) => {
            let diffX = targetX - this.x,
                diffY = targetY - this.y,
                d = p.dist(targetX, targetY, this.x, this.y);
            if (d > 1) {
                this.dX += (diffX / d);
                this.dY += (diffY / d);
            } else {
                this.dX = 0;
                this.dY = 0;
            }
        }

        this.swapVel = (other) => {
            this.dX = other.dX;
            this.dY = other.dY;
        return
        }
    }
}
