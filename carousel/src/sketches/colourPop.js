import Dot from './dot'
import _ from 'lodash'

export const colourPop = (p) => {
    p.socket = null;
    p.song = null;

    let canvas, row, target, targetColor, moves = 2, dots = [], bg, width = 35,
        tarR, tarG, tarB, score = 0., player, players = [], pop;

    p.preload = () => {
        pop = p.loadSound('sound/pops.mp3');
        p.song = p.loadSound('sound/loop.wav');
        bg = p.loadImage('images/colourpop.jpg');
    }

    p.setup = () => {
        p.socket = io('https://colourPopServer.com:443', {secure: true});
        p.song.play();
        p.song.loop();
        p.song.setVolume(0.1);
        p.createCanvas(800, 500);
        p.noStroke();
        p.textSize(20);
        p.textAlign(p.CENTER);
        row = p.createDiv('');
        row.addClass('row center-lg center-md center-sm center-xs');
        target = p.createDiv('TARGET COLOR');
        target.addClass('color');
        row.child(target);

        p.socket.on('connected', () => {
            p.socket.emit('start');
        })

        p.socket.on('startResponse', data => {
            tarR = data.colors[0];
            tarG = data.colors[1];
            tarB = data.colors[2];
            player = spawnDot(data.player.x, data.player.y,
                data.player.r, data.player.g, data.player.b, data.player.dX,
                data.player.dY, p.socket.id)
            for (let i = 0; i < data.dots.length; i++) {
                dots.push(spawnDot(data.dots[i].x, data.dots[i].y,
                    data.dots[i].r, data.dots[i].g, data.dots[i].b,
                    data.dots[i].dX, data.dots[i].dY));
            }
            for (let i = 0; i < data.players.length; i++) {
                players.push(spawnDot(data.players[i].x, data.players[i].y,
                    data.players[i].r, data.players[i].g, data.players[i].b,
                    data.players[i].dX, data.players[i].dY, data.players[i].id));
                }
            targetColor = p.color(data.colors[0], data.colors[1], data.colors[2]);
            target.style('background-color', targetColor);
            })

        p.socket.on('newPlayer', data => {
            let e = data.length-1;
            if (player) {
                if (data[e].id == player.id) return
                else {
                    players.push(spawnDot(data[e].x, data[e].y,
                        data[e].r, data[e].g, data[e].b,
                    data[e].dX, data[e].dY, data[e].id));
                }
            }
        })

        p.socket.on('playerLeft', id => {
            let i = _.findIndex(players, {id : id});
            players.splice(i, 1);
        })

        p.socket.on('heartBeat', data => {
            moves = data.moves;
            if (players.length > 0) {
                for (let i = 0; i < data.players.length; i++) {
                    players[i].x = data.players[i].x;
                    players[i].y = data.players[i].y;
                    players[i].w = data.players[i].w;
                    players[i].r = data.players[i].r;
                    players[i].g = data.players[i].g;
                    players[i].b = data.players[i].b;
                    players[i].textAlpha = data.players[i].textAlpha;
                    players[i].diff = data.players[i].diff;
                    players[i].dX = data.players[i].dX;
                    players[i].dY = data.players[i].dY;
                }
            }
            if (dots.length > 0) {
                for (let i = 0; i < data.dots.length; i++) {
                    dots[i].x = data.dots[i].x;
                    dots[i].y = data.dots[i].y;
                    dots[i].w = data.dots[i].w;
                    dots[i].radius = data.dots[i].radius;
                    dots[i].r = data.dots[i].r;
                    dots[i].g = data.dots[i].g;
                    dots[i].b = data.dots[i].b;
                    dots[i].a = data.dots[i].a;
                    dots[i].textAlpha = data.dots[i].textAlpha;
                    dots[i].diff = data.dots[i].diff;
                    dots[i].dX = data.dots[i].dX;
                    dots[i].dY = data.dots[i].dY;
                    dots[i].life = data.dots[i].life;
                    dots[i].isChanging = data.dots[i].isChanging;
                }
            }
        })

        p.socket.on('collision', dot => {
            player.swapVel(dot);
            player.move();
            let startChoice = p.random([2.5, 6.2, 8.8]);
            pop.play();
            pop.jump(startChoice, .3);
            if (!dot.id)
                blendColor(player, dot, false);
            else
                blendColor(player, dot, true);
        })

        p.socket.on('winner', (id) => {
            if (player) {
                if (id === player.id)
                    player.winner = true;
                else player.winner = false;
            }
        })

        p.socket.on('reset', () => {
            player = null;
            dots = [];
            players = [];
        })

    }

    p.draw = () => {
        p.background(bg);
        if (player) {
            p.fill('pink');
            player.drawRing();
            player.drawTail();
            player.move();
            player.show();
            player.drawColor();
            p.socket.emit('update', player);
            if (player.winner) {
                player.dX = 0;
                player.dY = 0;
                player.moveTo(p.width/2, p.height/2);
                player.textAlpha += 2;
                player.drawStats();
            }
        }
        for (let i = 0; i < players.length; i++) {
            if (players[i].id != player.id) {
                p.fill('tan');
                players[i].drawRing();
                players[i].drawTail();
                players[i].move();
                players[i].show();
                players[i].drawColor();
                players[i].drawStats();
            }
        }
        p.text(moves, p.width/2, p.height-5);
        for (let i = 0; i < dots.length; i++) {
            dots[i].drawTail();
            dots[i].move();
            dots[i].show();
        }
    }

    p.keyPressed = () => {
        let targetX,
            targetY,
            numPixels = 50;
        if (p.key === 'W') {
            targetX = player.x;
            targetY = player.y - numPixels;
            player.moveTo(targetX, targetY);
        }
        if (p.key === 'A') {
            targetX = player.x - numPixels;
            targetY = player.y;
            player.moveTo(targetX, targetY);
        }
        if (p.key === 'S') {
            targetY = player.y + numPixels;
            targetX = player.x;
            player.moveTo(targetX, targetY);
        }
        if (p.key === 'D') {
            targetY = player.y;
            targetX = player.x + numPixels;
            player.moveTo(targetX, targetY);
        }
  }

    function spawnDot(x, y, r, g, b, dx, dy, id) {
        let dot;
        if (!id) {id = null}
        if (!r && r !== 0) {r = p.round(p.random(255))}
        if (!g && g !== 0) {g = p.round(p.random(255))}
        if (!b && b !== 0) {b = p.round(p.random(255))}
        dot = new Dot(x, y, width, r, g, b, 255, tarR, tarG, tarB, dx, dy, id, p);
        return dot
    }

    function blendColor(obj1, obj2) {
        let from = p.color(obj1.r, obj1.g, obj1.b),
            to = p.color(obj2.r, obj2.g, obj2.b),
            blend = p.lerpColor(from, to, 0.5),
            bR = blend.levels[0],
            bG = blend.levels[1],
            bB = blend.levels[2];
            obj1.r = bR;
            obj1.g = bG;
            obj1.b = bB;
            obj1.setDiff();
        return
    }
}
