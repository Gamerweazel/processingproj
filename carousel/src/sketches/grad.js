import Slice from './slice'
import Rects from './rects'

export const grad = (p) => {
    let canvas, slices = [], rects = [], sliceIter = 360, rectIter = 6,
        startInput, endInput, amt, loopDist, dir, diff,
        x = 0, y, sliceStart = 0, sliceEnd = p.TWO_PI/sliceIter, boxes,
        startHue = 360, endHue = 180, height = 20, h = 1, s = 100, l = 50;

    p.setup = () => {
        p.noStroke();
        if (p.windowWidth >= 550 && p.windowHeight >= 800) {
            canvas = p.createCanvas(500, 500);
        } else {
            canvas = p.createCanvas(300, 300);
        }
        // startInput = p.createInput('#ff0000');
        // endInput = p.createInput('#00fcff');
        // startInput.attribute('type', 'color');
        // endInput.attribute('type', 'color');
        // startInput.addClass('center center-left');
        // endInput.addClass('center center-right');
        let row1 = p.createDiv(''), row2 = p.createDiv('');
        row1.addClass('row center-lg center-md center-sm center-xs');
        row2.addClass('row center-lg center-md center-sm center-xs');
        y = p.height-height;
        for (let i = 0; i < sliceIter; i++) {
            h = HueShift(h, 360/sliceIter);
            slices.push(new Slice(sliceStart, sliceEnd, h, s, l, p));
            sliceStart += p.TWO_PI/sliceIter;
            sliceEnd += p.TWO_PI/sliceIter;
        }
        let w = p.width/rectIter;
        for (let i = 0; i < rectIter; i++) {
            rects.push(new Rects(x, y, w, height, startHue, s, l, p));
            x += p.width/rectIter;
        }
        let makeChildren = (row) => {
            for (let i = 0; i < 3; i++) {
                let div = p.createDiv('');
                div.addClass('color');
                row.child(div);
            }
        }
        let makeList = (row) => {
            let lst = row.elt.children
            return [lst[0], lst[1], lst[2]]
        }
        makeChildren(row1);
        makeChildren(row2);
        boxes = makeList(row1).concat(makeList(row2));
    }

    p.draw = () => {
        p.background('#EDE1D1');
        // startHue = p.round(p.hue(p.color(startInput.value())));
        // endHue = p.round(p.hue(p.color(endInput.value())));
        let inc = dir === 'left' ? -1 : 1,
            j = 0;
        for (let i = startHue; j <= amt*rectIter; i+=inc) {
            let changed = false;
            if (i > sliceIter-1 && !changed) {
                i = 0;
                changed = true;
            } else if (i < 0 && !changed) {
                i = sliceIter-1;
                changed = true;
            }
            slices[i].show();
            j++;
        }
        let w = p.width / rectIter,
            diff = startHue - endHue;
        if (startHue < endHue) {
            loopDist = (-startHue) - (sliceIter - endHue);
            dir = p.abs(loopDist) < p.abs(diff) ? 'left' : 'right';
            amt = dir === 'left' ? p.abs(loopDist) : p.abs(diff);
        } else {
            loopDist = p.abs(endHue + sliceIter) - startHue;
            dir = p.abs(loopDist) < p.abs(diff) ? 'right' : 'left';
            amt = dir === 'right' ? p.abs(loopDist) : p.abs(diff);
        }
        amt /= rectIter;
        boxes[0].style.backgroundColor = `hsl(${startHue}, 100%, 50%)`;
        boxes[0].innerText = boxes[0].style.backgroundColor;
        boxes[5].style.backgroundColor = `hsl(${endHue}, 100%, 50%)`;
        boxes[5].innerText = boxes[5].style.backgroundColor;
        let shiftStart = startHue;
        for (let i = 1; i < boxes.length-1; i++) {
            let hue = dir === 'left' ? HueShift(shiftStart, -amt) : HueShift(shiftStart, amt);
            shiftStart = hue;
            boxes[i].style.backgroundColor = `hsl(${p.round(shiftStart)}, 100%, 50%)`;
            boxes[i].innerText = boxes[i].style.backgroundColor;
        }
    }

    p.mouseDragged = () => {
        startHue = p.round(HueShift(startHue, p.mouseX - p.pmouseX));
        endHue = p.round(HueShift(endHue, p.mouseY - p.pmouseY));
    }

    function HueShift(h,s) {
        h+=s; while (h>=360.0) h-=360.0; while (h<0.0) h+=360.0; return h;
    }

}
