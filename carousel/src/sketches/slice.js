export default class Slice {
    constructor(start, fin, h, s, l, p) {
        this.x = p.width / 2;
        this.y = p.height / 2;
        this.start = start;
        this.fin = fin;
        this.h = h;
        this.s = s;
        this.l = l;

        this.getColors = () => {
            return [this.h, this.s, this.l];
        }
        this.show = () => {
            p.fill('hsl('+this.h+','+this.s+'%,'+this.l+'%)');
            p.arc(this.x, this.y, p.width, p.height, this.start, this.fin);
        }
        this.spin = (d, a) => {
            if (d >= 1) {
                this.start += a;
                this.fin += a;
            } else {
                this.start -= a;
                this.fin -= a;
            }
        }
        this.drag = (d) => {
            if (d >= 1) {
                this.start += 0.1;
                this.fin += 0.1;
            } else {
                this.start -= 0.1;
                this.fin -= 0.1;
            }
        }
        this.setColor = (h, s, l) => {
            this.h = h;
            this.s = s;
            this.l = l;
        }
        this.positionPop = () => {
            this.y = p.height / 2 + 25;
        }
        this.positionReset = () => {
            this.y = p.height / 2;
        }
    }
}
