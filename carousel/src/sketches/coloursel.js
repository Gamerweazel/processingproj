import Slice from './slice'

export const coloursel = (p) => {
    p.menuOpen = false;

    let slices = [], start = 0, fin = 1.0472, amt, spin = false, direction = 1,
        boxes, row1, row2, canvas, pixels, wheelClicks;

    p.preload = () => {
        wheelClicks = p.loadSound('sound/spokesm4v.mp3');
    }

    p.setup = () => {
        if (p.windowWidth >= 550 && p.windowHeight >= 800) {
            canvas = p.createCanvas(500, 500);
        } else {
            canvas = p.createCanvas(300, 300);
        }
        p.noStroke();
        p.setShakeThreshold(60);
        let h = p.round(p.random(360));
        for (let i = 0; i < 6; i++) {
            h = HueShift(h, 5);
            slices.push(new Slice(start, fin, h, 100, 50-(i*5), p));
            start += 1.0472;
            fin += 1.0472;
        }
        row1 = p.createDiv(''), row2 = p.createDiv('');
        row1.addClass('row center-lg center-md center-sm center-xs');
        row2.addClass('row center-lg center-md center-sm center-xs');
        let makeChildren = (row) => {
            for (let i = 0; i < 3; i++) {
                let div = p.createDiv('');
                div.addClass('color');
                div.elt.oncontextmenu = function() {
                    let colors = this.style.backgroundColor,
                    h = p.hue(colors.split("(")[1].split(")")[0].split(","));
                    for (let j = 0; j < slices.length; j++) {
                        slices[j].setColor(p.round(h), 100, 50-(j*5));
                    }
                }
                row.child(div);
            }
        }
        let makeList = (row) => {
            let lst = row.elt.children
            return [lst[0], lst[1], lst[2]]
        }
        makeChildren(row1);
        makeChildren(row2);
        boxes = makeList(row1).concat(makeList(row2));
        let clip = new Clipboard('.color', {
            text: function(trigger) {
                return trigger.innerText
            }
        })
    }

    p.draw = () => {
        if (p.windowWidth >= 550 && p.windowHeight >= 800) {
            pixels = p.get(p.width / 2, 415);
        } else {
            pixels = p.get(p.width / 2, 215);
        }
        let h = p.round(p.hue(pixels));
        if (!spin) {
            let s = p.saturation(pixels),
                l = p.lightness(pixels),
                hc = HueShift(h, 180.0),
                a1 = HueShift(h, 30.0),
                a2 = HueShift(h, -30.0),
                a3 = HueShift(hc, 30.0),
                a4 = HueShift(hc, -30.0);
            boxes[1].style.backgroundColor = `hsl(${h}, ${s}%, ${l}%)`;
            boxes[0].style.backgroundColor = `hsl(${a1}, ${s}%, ${l}%)`;
            boxes[2].style.backgroundColor = `hsl(${a2}, ${s}%, ${l}%)`;
            boxes[4].style.backgroundColor = `hsl(${hc}, ${s}%, ${l}%)`;
            boxes[3].style.backgroundColor = `hsl(${a3}, ${s}%, ${l}%)`;
            boxes[5].style.backgroundColor = `hsl(${a4}, ${s}%, ${l}%)`;
        } else {
            if (amt <= 0.01) {
                amt = 0;
                spin = false;
            }
            if (p.frameCount % 6 === 0) {
                amt *= 0.9;
            }
        }
        p.background('#EDE1D1');
        let rh = p.round(p.random(360));
        for (let i = 0; i < slices.length; i++) {
            slices[i].show();
            slices[i].positionReset();
            boxes[i].innerText = boxes[i].style.backgroundColor;
            if (spin) {
                slices[i].spin(direction, amt);
                if (amt >= 1) {
                    rh = HueShift(rh, 5);
                    slices[i].setColor(rh, 100, 50-(i*5));
                }
            }
        }
    }

    p.mousePressed = () => {
        amt = 0;
        spin = false;
    }

    p.deviceShaken = () => {
        amt = 2;
        spin = true;
        if (!wheelClicks.isPlaying() && spin) {
            wheelClicks.play();
        }
    }

    p.touchEnded = () => {
        wheelClicks.stop();
        if (!p.menuOpen && p.touchY > 20 && p.touchX > 20) return false
    }

    p.mouseDragged = () => {
        if (p.mouseY < 0 || p.mouseY > p.height) return
        if (p.mouseX < 0 || p.mouseX > p.width) return
        if (p.mouseX < p.width / 2) {
            if (p.mouseY < p.pmouseY) {
                direction = 1;
            } else {
                direction = -1;
            }
        } else {
            if (p.pmouseY < p.mouseY) {
                direction = 1;
            } else {
                direction = -1;
            }
        }
        for (let i = 0; i < slices.length; i++) {
            slices[i].drag(direction);
        }
        return false;
    }

    p.mouseWheel = (event) => {
        let d = event.delta;
        for (let i = 0; i < slices.length; i++) {
            slices[i].drag(d);
        }
    }

    p.mouseReleased = () => {
        if (p.mouseY < 0 || p.mouseY > p.height) return
        if (p.mouseX < 0 || p.mouseX > p.width) return
        amt = Math.abs(p.mouseY - p.pmouseY);
        if (amt > 0) {
            spin = true;
        }
        if (!wheelClicks.isPlaying() && spin) {
            wheelClicks.play();
        } else {
            wheelClicks.stop();
        }
    }

    function HueShift(h,s) {
        h+=s; while (h>=360.0) h-=360.0; while (h<0.0) h+=360.0; return h;
    }
}
