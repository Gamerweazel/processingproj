export default class Rects {
    constructor(x, y, w, height, hue, s, l, p) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.height = height;
        this.hue = hue;
        this.s = s;
        this.l = l;

        this.getColors = () => {
            return [this.hue, this.s, this.l]
        }

        this.show = () => {
            p.fill('hsl('+this.hue+','+this.s+'%,'+this.l+'%)');
            p.rect(this.x, this.y, this.w, this.height);
        }

        this.setColor = (h, s, l) => {
            this.hue = h;
            this.s = s;
            this.l = l;
        }
    }
}
