import React, { Component } from 'react'

export default class P5Component extends Component {
    constructor(props) {
        super(props)
        this.state = {
          sketch: this.props.sketch
        }
    }

    renderNewSketch() {
        this.sketch = new p5(this.state.sketch, this.refs.sketch);
    }

    componentWillReceiveProps(nextProps) {
        if (this.sketch.socket)
            this.sketch.socket.disconnect();
        if (this.sketch.song)
            this.sketch.song.stop();
        if (nextProps.sketch !== this.state.sketch) {
            this.sketch.remove();
            this.setState({sketch: nextProps.sketch}, this.renderNewSketch);
        }
        if (nextProps.menuOpen !== this.sketch.menuOpen)
            this.sketch.menuOpen = nextProps.menuOpen;
    }

    componentDidMount() {
        this.renderNewSketch();
    }

    render() {
        return(
            <div className='row center-lg center-md center-sm center-xs'>
                <div id='sketch' ref='sketch' />
            </div>
        )
    }
}
