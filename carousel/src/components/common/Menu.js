import React, { Component } from 'react'

export default class Menu extends Component {
    constructor(props) {
        super(props)
        this.hide = this.hide.bind(this)
        this.state = {
            visible: false
        }
    }

    show() {
        this.setState({visible: true});
        document.addEventListener("click", this.hide);
    }

    hide() {
        this.setState({visible: false});
        document.removeEventListener("click", this.hide);
    }

    render() {
        return (
            <div className={this.state.visible ? "menu" : "menu notVisible"}>{this.props.children}</div>
        )
    }
}
