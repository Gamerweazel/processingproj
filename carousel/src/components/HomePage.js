import React, { Component } from 'react'
import P5Component from './P5Component'
import { coloursel } from '../sketches/coloursel'
import { grad } from '../sketches/grad'

export default class HomePage extends Component {
    constructor() {
        super()
        this.changeSketch = this.changeSketch.bind(this)
        this.state = {
            selectedSketch: coloursel
        }
    }

    changeSketch() {
        let sketch = this.state.selectedSketch == coloursel ? grad : coloursel;
        this.setState({selectedSketch: sketch});
    }

    render() {
        return(
            <div>
                <P5Component sketch={this.state.selectedSketch} />
            </div>
        )
    }
}
