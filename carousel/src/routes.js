import { Route, IndexRoute } from 'react-router'
import React from 'react'
import App from './App'

export const routes = (
    <Route path='/' component={App}></Route>
)
