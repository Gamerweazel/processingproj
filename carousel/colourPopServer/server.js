'use strict'

const app = require('express')();
const _ = require('lodash');
const fs = require('fs');
const https = require('https');
const options = {
    key: fs.readFileSync('./secure/privatekey.pem'),
    ca: fs.readFileSync('./secure/colourpopserver_com.ca-bundle'),
    cert: fs.readFileSync('./secure/colourpopserver_com.crt')
}
const server = https.createServer(options, app);
const io = require('socket.io')(server);
const port = 443;

server.listen(port, err => {
    if (err) console.log(err);
    else console.log(`\ncolourPopServer listening on ${port}`);
});

// game logic
let dots = [],
    players = [],
    colors = [],
    gameOver = false,
    moves = 2,
    time = 0,
    timeOut = 1800,
    winner = {id: null, diff: 755},
    targetR,
    targetG,
    targetB;

function Dot(x, y, r, g, b, id) {
    this.x = x;
    this.y = y;
    this.w = 35;
    this.radius = this.w/2;
    this.id = id;
    this.r = r;
    this.g = g;
    this.b = b;
    this.drag = 0.1
    this.a = 255;
    this.textAlpha = -100;
    this.diff = 255;
    this.dX = Math.floor(Math.random()*2) == 1 ? 1 : -1;
    this.dY = Math.floor(Math.random()*2) == 1 ? 1 : -1;
    this.life = 1;
    this.isChanging = false;

    this.move = () => {
        this.x += this.dX;
        this.y += this.dY;
        if (this.x < this.radius) {
            this.x = this.radius;
            this.dX = -this.dX * this.drag;
        } else if (this.x > 800 - this.radius) {
            this.x = 800 - this.radius;
            this.dX = -this.dX * this.drag;
        } else if (this.y < this.radius) {
            this.y = this.radius
            this.dY = -this.dY * this.drag;
        } else if (this.y > 500 - this.radius) {
            this.y = 500 - this.radius
            this.dY = -this.dY * this.drag
        }
        return
    }
    this.moveTo = (targetX, targetY) => {
        let diffX = targetX - this.x,
            diffY = targetY - this.y,
            d = Math.sqrt(Math.pow(diffX, 2) + Math.pow(diffY, 2));
        if (d > 0) {
            this.dX += (diffX / d);
            this.dY += (diffY / d);
        } else {
            this.dX = 0;
            this.dY = 0;
        }
    }
    this.hits = (other) => {
        return (Math.sqrt(Math.pow(this.x - other.x, 2) +
            Math.pow(this.y-other.y, 2)) < this.radius + other.radius);
    }
    this.swapVel = (other) => {
        let tmp = this.dX;
        this.dX = other.dX;
        other.dX = tmp;

        tmp = this.dY;
        this.dY = other.dY;
        other.dY = tmp;
    return
    }
}

let setUp = () => {
    let redDot = new Dot(500, 39, 255, 0, 0),
        greenDot = new Dot(500, 100, 0, 255, 0),
        blueDot = new Dot(500, 150, 0, 0, 255),
        blackDot = new Dot(500, 200, 0, 0, 0),
	whiteDot = new Dot(500, 250, 255, 255, 255);
    targetR = Math.floor((Math.random()*255) + 1);
    targetG = Math.floor((Math.random()*255) + 1);
    targetB = Math.floor((Math.random()*255) + 1);
    colors.push(targetR);
    colors.push(targetG);
    colors.push(targetB);
    dots.push(redDot);
    dots.push(greenDot);
    dots.push(blueDot);
    dots.push(blackDot);
    dots.push(whiteDot);
}
let main = () => {
    for (let i = 0; i < dots.length; i++) {
        dots[i].move();
        if (dots[i].isChanging) {
            dots[i].w--;
            dots[i].a-=5;
            let x = Math.floor((Math.random()*700) + 1),
                y = Math.floor((Math.random()*700) + 1);
            dots[i].moveTo(x, y);
            if (time % 360 === 0) {
                dots[i].life = 1;
                dots[i].a = 255;
                dots[i].w = 35;
                dots[i].isChanging = false;
            }
        }
        for (let j = i + 1; j < dots.length; j++) {
            if (dots[i].hits(dots[j])) {
                dots[i].swapVel(dots[j]);
                dots[i].move()
                dots[j].move()
            }
        }
        for (let k = 0; k < players.length; k++) {
            if (dots[i].hits(players[k]) && !gameOver && dots[i].life > 0) {
                io.sockets.in(players[k].id).emit('collision', dots[i]);
                dots[i].swapVel(players[k]);
                dots[i].move();
                moves--;
                dots[i].life--;
                if (dots[i].life == 0) dots[i].isChanging = true
                if (moves === 0) gameOver = true;
            }
            if (players.length > 1) {
                for (let l = k + 1; l < players.length; l++) {
                    if (players[l].hits(players[k]) && !gameOver) {
                       // io.sockets.in(players[k].id).emit('collision', players[l]);
                       // io.sockets.in(players[l].id).emit('collision', players[k]);
                    }
                }
            }
        }
        if (gameOver) {
            dots[i].a--;
            io.sockets.emit('winner', winner.id);
            if (timeOut == -1500)
            	io.sockets.emit('reset');
            if (timeOut == -1800)
            	process.exit(1);
            timeOut--;
        }
    }
}

let resetDots = () => {
    for (let i = 0; i < dots.length; i++) {
        dots[i].x = Math.floor((Math.random()*500) + 1);
        dots[i].y = Math.floor((Math.random()*500) + 1);
        dots[i].dX = 1;
        dots[i].dY = 1;
    }
}

let calcWinner = () => {
    for (let i = 0; i < players.length; i++) {
        if (players[i].diff < winner.diff) {
            winner = players[i];
        }
    }
}

setUp();

setInterval(() => {
    main();
    time++
    io.sockets.emit('heartBeat', {dots, players, moves})
    }, 16.67);

io.on('connection', socket => {
    console.log(`client ${socket.id} has connected`);
    socket.emit('connected');

    socket.on('start', () => {
        let x;
        if (players.length == 0) x = 30
        else x = 700;
        let player = new Dot(x, 300, 255, 255, 255, socket.id);
        players.push(player);
        io.sockets.emit('newPlayer', players);
        socket.emit('startResponse', {dots, players, colors, player});
    })

    socket.on('update', data => {
        let i = _.findIndex(players, {id : socket.id});
        if (i != -1) {
            players[i].x = data.x;
            players[i].y = data.y
            players[i].dX = data.dX;
            players[i].dY = data.dY;
            players[i].r = data.r;
            players[i].g = data.g;
            players[i].b = data.b;
            players[i].diff = data.diff;
            players[i].textAlpha = data.textAlpha;
        }
        calcWinner();
    })

    socket.on('disconnect', () => {
        console.log(`client ${socket.id} has disconnected`);
        let i = _.findIndex(players, {id : socket.id});
        players.splice(i, 1);
        io.sockets.emit('playerLeft', socket.id);
    })
});
