self.addEventListener('install', function(e) {
    e.waitUntil(
        caches.open('default-cache').then(function(cache) {
            return cache.addAll([
                '/',
                '/index.html',
                '/bundle.js',
                '/libraries/p5.dom.min.js',
                '/libraries/p5.min.js',
                '/libraries/p5.sound.min.js',
                '/libraries/clipboard.min.js',
                '/sound/spokesm4v.mp3',
                '/css/flexboxgrid.min.css',
                '/images/icon-192.png',
                '/manifest.json',
            ]);
        }).then(function() {
            return self.skipWaiting();
        }));
});

self.addEventListener('activate', function(e) {
    e.waitUntil(self.clients.claim());
});

self.addEventListener('fetch', function(e) {
    e.respondWith(
        caches.match(e.request).then(function(response) {
            return response || fetch(e.request)
        })
    )
});
