const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: [
        './src/Main'
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    watch: true,
    debug: true,
    devtool: 'source-map',
    module: {
        loaders: [{
            loader: 'babel-loader',
            include: [
                path.resolve(__dirname, 'src')
            ],
            test: /\.jsx?$/,
            query: {
                presets: ['es2015', 'react']
            }
        }]
    },
    plugins:[
    new webpack.DefinePlugin({
      'process.env':{
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress:{
        warnings: true
      }
    })
  ]
};
