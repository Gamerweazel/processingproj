class Bird {
    constructor() {
        this.y = height / 2
        this.x = 64
        this.gravity = 0.6
        this.lift = -18
        this.velocity = 0
        this.show = () => {
            fill(255)
            ellipse(this.x, this.y, 32, 32)
        }
        this.update = () => {
            this.velocity = this.velocity + this.gravity
            this.velocity *= 0.9
            this.y = this.y + this.velocity

            if (this.y < 16) {
                this.y = 16
                this.velocity = 0
            }
        }
        this.up = () => {
            this.velocity += this.lift
        }
    }
}
