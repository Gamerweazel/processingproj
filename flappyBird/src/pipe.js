class Pipe {
    constructor() {
        this.top = random(100, 280)
        this.bottom = random(100, 280)
        this.x = width
        this.w = 20
        this.speed = 2
        this.highlight = false
        this.show = () => {
            fill(255)
            if (this.highlight) {
                fill(255, 0, 0)
            }
            rect(this.x, 0, this.w, this.top)
            rect(this.x, height - this.bottom, this.w, this.bottom)
        }
        this.update = () => {
            this.x -= this.speed
        }
        this.offScreen = () => {
            return this.x < -this.w
        }
        this.hits = (bird) => {
            if (bird.y - 16 < this.top || bird.y + 16 > height - this.bottom) {
                if (bird.x + 16 > this.x && bird.x - 16 < this.x + this.w) {
                    this.highlight = true
                    return true
                }
            }
            this.highlight = false
            return false
        }
    }
}
