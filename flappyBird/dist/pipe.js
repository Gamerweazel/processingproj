"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Pipe = function Pipe() {
    var _this = this;

    _classCallCheck(this, Pipe);

    this.top = random(100, 280);
    this.bottom = random(100, 280);
    this.x = width;
    this.w = 20;
    this.speed = 2;
    this.highlight = false;
    this.show = function () {
        fill(255);
        if (_this.highlight) {
            fill(255, 0, 0);
        }
        rect(_this.x, 0, _this.w, _this.top);
        rect(_this.x, height - _this.bottom, _this.w, _this.bottom);
    };
    this.update = function () {
        _this.x -= _this.speed;
    };
    this.offScreen = function () {
        return _this.x < -_this.w;
    };
    this.hits = function (bird) {
        if (bird.y - 16 < _this.top || bird.y + 16 > height - _this.bottom) {
            if (bird.x + 16 > _this.x && bird.x - 16 < _this.x + _this.w) {
                _this.highlight = true;
                return true;
            }
        }
        _this.highlight = false;
        return false;
    };
};