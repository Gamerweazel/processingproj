"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Bird = function Bird() {
    var _this = this;

    _classCallCheck(this, Bird);

    this.y = height / 2;
    this.x = 64;
    this.gravity = 0.6;
    this.lift = -18;
    this.velocity = 0;
    this.show = function () {
        fill(255);
        ellipse(_this.x, _this.y, 32, 32);
    };
    this.update = function () {
        _this.velocity = _this.velocity + _this.gravity;
        _this.velocity *= 0.9;
        _this.y = _this.y + _this.velocity;

        if (_this.y < 16) {
            _this.y = 16;
            _this.velocity = 0;
        }
    };
    this.up = function () {
        _this.velocity += _this.lift;
    };
};