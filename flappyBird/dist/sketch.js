'use strict';

var bird = void 0;
var pipes = [];
var num = 0;

function setup() {
    createCanvas(400, 600);
    bird = new Bird();
    pipes.push(new Pipe());
}

function draw() {
    background(105);
    fill(220);
    text(num, 10, 30);
    for (var i = pipes.length - 1; i >= 0; i--) {
        pipes[i].show();
        pipes[i].update();
        if (pipes[i].hits(bird)) {
            num -= 10;
        }
        if (pipes[i].offScreen()) {
            pipes.splice(i, 1);
            num += 20;
        }
    }
    bird.update();
    bird.show();
    if (frameCount % 100 === 0) {
        pipes.push(new Pipe());
    }
}

function keyPressed() {
    if (key === ' ') {
        bird.up();
    }
}